/*5)  Escreva  um  programa  para  ler  as  dimensões  de 
 *           uma  cozinha  retangular  (comprimento,  largura  e altura), 
 *            calcular  e  escrever  a  quantidade  de  caixas  de  azulejos  
 *            para  se  colocar  em  todas  as  suas paredes  (considere  quenão
 *              será  descontada  a  área  ocupada  por  portas  e  janelas). 
 *               Cada  caixa  de azulejos possui 1,5 m2. */
import java.util.Scanner;
public class EX5 {

	public static void main(String[] args) {
		Scanner tecla = new Scanner(System.in);
		double c, l, h, l1, l2, t;
		int tca;
		final double ca = 1.5;
		System.out.println("Digite o comprimento: ");
		c = tecla.nextDouble();
		System.out.println("Digite a largura: ");
		l = tecla.nextDouble();
		System.out.println("Digite a altura: ");
		h = tecla.nextDouble();
		l1 = (h*c)*2;
		l2 = (h*l)*2;
		t= l1 + l2;
		tca = ((int)(t / ca))+1;
		System.out.println("O cômodo vai necessitar de "+tca+" caixas de azuleijo!");		

	}

}
