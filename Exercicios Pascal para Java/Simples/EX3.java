import java.util.Scanner;

/*3)  Escreva  um  programa  para  ler  uma  temperatura 
 *    em  graus  Celsius,  calcular  e  escrever  o  valor 
 *    correspondente em graus Fahrenheit. */

public class EX3 {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		double f, c;
		System.out.println("Digite a temperatura em Celsius: ");
		c = tecla.nextDouble();
		f= (c * 9/5) + 32;
		System.out.println("A temperatura em graus Fahrenheit é: "+f+"Fº");

	}

}
