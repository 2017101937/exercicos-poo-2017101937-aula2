/*1) Escreva um programa para ler o raio de um círculo, calcular
 *  e escrever a sua área. πR2
 */
import java.util.Scanner;

public class EX1 {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		double r, a;
		final double pi = 3.14;
		System.out.println("Informe o valor do Raio do círculo: ");
		r = tecla.nextDouble();
		a = (r * r)*pi;
		System.out.println("O valor da area é: "+a+"m²");

	}

}
