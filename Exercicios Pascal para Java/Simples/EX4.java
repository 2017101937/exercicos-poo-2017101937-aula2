/*  4) Escreva um programa para calcular e imprimir o número de 
 *    lâmpadas necessárias para iluminar um determinado  cômodo  de 
 *     uma  residência.  Dados  de  entrada:  a  potência  da  lâmpada 
 *      utilizada  (em watts),  as  dimensões  (largura  e  comprimento,
 *        em  metros)  do  cômodo.Considere  que  a  potência necessária
 *         é de 18 watts por metro quadrado. */
import java.util.Scanner;
public class EX4 {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		double p, l, c;
		int n;
		System.out.println("Digite a Potência da lâmpada em Watts: ");
		p = tecla.nextDouble();
		System.out.println("Digite o comprimento do cômodo em Metros: ");
		c = tecla.nextDouble();
		System.out.println("Digite a largura do cômodo em Metros: ");
		l = tecla.nextDouble();
		n = ((int) ((l*c)*(18/p)))+1;
		System.out.println("O numero necessário de lâmpadas é: "+n);
		

	}

}
