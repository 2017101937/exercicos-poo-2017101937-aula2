/* 2)  Escreva  um  programa  para  ler  *   uma  temperatura 
 *   em  graus  Fahrenheit,  calcular  e  escrever  
 *   o  valor correspondente em graus Celsius. */
import java.util.Scanner;
public class EX2 {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		double f, c;
		System.out.println("Digite a temperatura em Fahrenheit: ");
		f = tecla.nextDouble();
		c= (f-32)*5/9;
		System.out.println("A temperatura em graus celsius é: "+c+"cº");	

	}

}
