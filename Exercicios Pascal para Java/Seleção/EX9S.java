/*9) As maçãs custam R$ 0,30 cada se forem compradas menos do que uma dúzia,
 *  e R$ 0,25 se forem
compradas pelo menos doze. Escreva um programa que leia o número de
 maçãs compradas,
calcule
*/
import java.util.Scanner;
public class EX9S {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		int maças;
		double p, pfin;
		System.out.println("Digite quantas maçãs serão compradas: ");
		maças = tecla.nextInt();
		if(maças>=12) {
			p = 0.25;
			pfin = maças * p;
			}else {
				p = 0.3;
				pfin = maças * p;
			}
		System.out.println("Valor a pagar: R$"+pfin+" por "+maças+" Maçãs!");	

	}

}
