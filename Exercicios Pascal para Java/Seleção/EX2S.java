/*2) Escreva um programa que leia as notas das duas avaliações normais e a 
 * nota da avaliação optativa.Caso o aluno não tenha feito a optativa 
 * deve ser fornecido o valor –1. Calcular a média do semestre
considerando que a prova optativa substitui a nota mais baixa entre as
 duas primeiras avaliações.Escrever a média e mensagens que indiquem se 
 o aluno foi aprovado, reprovado ou está em exame,de acordo com as 
 informações abaixo:
Aprovado : media >= 6.0
Reprovado: media < 3.0
Exame
: media >= 3.0 e < 6.0
*/
import java.util.Scanner;
public class EX2S {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		double n1, n2, n3, m;
		final int np = 2;
		System.out.println("Digite a nota da Av1: ");
		n1 = tecla.nextDouble();
		System.out.println("Digite a nota da Av2: ");
		n2 = tecla.nextDouble();
		System.out.println("Digite a nota da Av3, caso não tenha feito digite -1: ");
		n3 = tecla.nextDouble();
		if(n3 == -1) {
			n3 = 0;
		}
		if(n3>n1) {
			n1 = n3;
		}else
			if(n3>n2) {
				n2 = n3;
			}
		m = (n1 + n2) / np;
		
		if(m>=6) {
			System.out.println("Aprovado!");
		}else
			if(m<3) {
				System.out.println("Reprovado!");
			}else {
				System.out.println("Exame final!");
			}

	}

}
