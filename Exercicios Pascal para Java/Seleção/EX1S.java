/*1) Escreva um programa que leia o código de origem de um produto e imprima
 *  na tela a região de sua
procedência conforme a tabela abaixo:
código 1 : Sul 
código 5 ou 6
: Nordeste
código 2 : Norte código 7, 8 ou 9 : Sudeste
código 3 : Leste código 10 : Centro-Oeste
código 4 : Oeste código 11 : Noroeste
Observação: Caso o código não seja nenhum dos especificados o produto deve 
ser encarado como
Importado.
*/
import java.util.Scanner;
public class EX1S {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		int cod;
		System.out.println("Digite o código: ");
		cod = tecla.nextInt();
		if (cod == 1) {
			System.out.println("O produto é da região Sul!");
		}else 
			if(cod == 2){
				System.out.println("O produto é da região Norte!");				
			}else
				if(cod == 3) {
					System.out.println("O produto é da região Leste!");
				}else
					if(cod == 4) {
						System.out.println("O produto é da região Oeste!");
					}else
						if((cod == 5) || (cod == 6)) {
							System.out.println("O produto é da região Nordeste!");
						}else
							if(((cod == 7) || (cod == 8)) || (cod ==9)) {
								System.out.println("O produto é da região Sudeste!");
							}else
								if(cod == 10) {
									System.out.println("O produto é da região Centro-Oeste!");
								}else 
									if(cod == 11) {
									System.out.println("O produto é da região Noroeste!");
									}else {
										System.out.println("O produto é Importado!");
									}
	}

}
