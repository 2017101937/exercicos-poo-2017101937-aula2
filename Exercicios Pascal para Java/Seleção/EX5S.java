/*
5) Escreva um programa para ler um valor e escrever se é positivo ou
 negativo. Considere o valor
zero como positivo.
 */
import java.util.Scanner;
public class EX5S {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		double n;
		System.out.println("Digite um valor: ");
		n = tecla.nextDouble();
		if(n>=0) {
			System.out.println("Valor positivo!");
		}else {
			System.out.println("Valor negativo!");
		}

	}

}
