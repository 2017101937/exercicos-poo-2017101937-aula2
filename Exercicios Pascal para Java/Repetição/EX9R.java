/*9) Escreva um programa para ler 2 notas de um aluno, calcular e imprimir 
 * a média final. Logo após escrever a mensagem "Calcular a média de outro 
 * aluno [S]im [N]ão?" e solicitar um resposta. Se a resposta for "S", o 
 * programa deve ser executado novamente, caso contrário deve ser encerrado
imprimindo a quantidade de alunos aprovados.
*/
import java.util.Scanner;
public class EX9R {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		String resp;
		double n1, n2, mf;
		final int tn = 2;
		int aa = 0;
		Object i;
		do {
			do {
				System.out.println("entre com a nota da av1");
				n1 = tecla.nextDouble();
				if((10<n1)||(n1<0)) {
					System.out.println("Nota de av1 Inválida!");
																	
				}
			}while((10<n1)||(n1<0));
			do {
				System.out.println("entre com a nota da av2");
				n2 = tecla.nextDouble();
				if((10<n2)||(n2<0)) {
					System.out.println("Nota de av2 Inválida!");
																	
				}
			}while((10<n2)||(n2<0));
			mf = (n1 + n2)/ tn;
			System.out.println("Sua média final é: "+mf);
			if(mf>=7) {
				aa = aa + 1;
			}
			System.out.println("Deseja continuar? s ou n: ");
			resp = tecla.next();
		}while(resp.equals("s"));
		System.out.println("Total de aprovados: "+aa);
	}

}
