/*13) Escreva um programa que leia o primeiro nome e a altura das moças
 *  inscritas em um concurso de
beleza. Quando for informada a palavra FIM para o nome da moça o programa
 deverá ser encerrado
e imprimir: o nome e a altura da moça mais alta e o número de moças no 
concurso. Considere que
todas as moças possuem altura diferente.
*/
import java.util.Scanner;
public class EX13R {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		double h, mh = 0;
		String nomea, nomemalt = null;
		int i = 0;
		
		do {
			System.out.println("Informe o nome da mulher: ");
			nomea = tecla.next();
			if(nomea.equals("fim")) {
				break;
			}
			System.out.println("Informe a altura da mulher: ");
			h = tecla.nextDouble();
			if(h>mh) {
				nomemalt = nomea;
				mh = h;
				}
			i = i +1;
			
		}while(!nomea.equalsIgnoreCase("fim"));
		System.out.println("O nome da moça mais alta é "+nomemalt+" e sua altura é "+mh+" metros");
		System.out.println("O concurso conta com "+i+" garotas participando!");
	}
}

