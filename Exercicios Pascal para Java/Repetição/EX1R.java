/*1) Para que a divisão entre 2 números possa ser realizada, o divisor 
 * não pode ser nulo (zero). Escreva um programa para ler 2 valores 
 * e imprimir o resultado da divisão do primeiro pelo segundo. OBS: O
programa deve validar a leitura do segundo valor (que não deve ser nulo).
 Enquanto for fornecido um valor nulo a leitura deve ser repetida.
  Utilize a estrutura Repita/Até na construção da repetição de validação
*/
import java.util.Scanner;
public class EX1R {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		double v1, v2, vf;
		System.out.println("Digite o valor a ser dividido: ");
		v1 = tecla.nextDouble();
		do {
		System.out.println("Digite o valor que irá dividir: ");
		v2 = tecla.nextDouble();
		}while(v2 == 0);	
		vf = v1 / v2;	
		System.out.println("valor é: "+vf);

	}

}
