/*5) Escreva um programa para ler as notas da 1a e 2a avaliações de um
 *  aluno, calcular e imprimir a
média semestral. Faça com que o algoritmo só aceite notas válidas
 (uma nota válida deve pertencer
ao intervalo [0,10]). Cada nota deve ser validada separadamente.
 Deve ser impressa a mensagem
"Nota inválida" caso a nota informada não pertença ao intervalo [0,10].
*/
import java.util.Scanner;
public class EX5R {

	public static void main(String[] args) {
		Scanner tecla = new Scanner (System.in);
		double n1, n2, mf;
		final int tn = 2;
		do {
			System.out.println("entre com a nota da av1");
			n1 = tecla.nextDouble();
			if((10<n1)||(n1<0)) {
				System.out.println("Nota de av1 Inválida!");
																
			}
		}while((10<n1)||(n1<0));
		do {
			System.out.println("entre com a nota da av2");
			n2 = tecla.nextDouble();
			if((10<n2)||(n2<0)) {
				System.out.println("Nota de av2 Inválida!");
																
			}
		}while((10<n2)||(n2<0));
		mf = (n1 + n2)/ tn;
		System.out.println("Sua média final é: "+mf);

	}

}
