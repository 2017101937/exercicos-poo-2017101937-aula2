/*1 2) A Federação Gaúcha de Futebol contratou você para escrever um programa 
 * para fazer uma estatística do resultado de vários GRENAIS. Escreva um 
 * algoritmo para ler o número de gols marcados pelo Inter, o número de gols
 * marcados pelo GRÊMIO em um GRENAL, imprimindo o nome do time vitorioso ou 
 * a palavra EMPATE. Logo após escrever a mensagem "Novo GRENAL 1.Sim 2.Não?"
 *  e solicitar uma resposta. Se a resposta for 1, o algoritmo deve ser executado
novamente solicitando o número de gols marcados pelos times em uma nova partida,
 caso contrário deve ser encerrado imprimindo:
- Quantos GRENAIS fizeram parte da estatística.
- O número de vitórias do Inter.
- O número de vitórias do Grêmio.
- O número de Empates.
- Uma mensagem indicando qual o time que venceu o maior número de GRENAIS (ou NÃO
HOUVE VENCEDOR).
Observação: Para implementar um contador em Pascal (incrementar):
variável := variável + 1
*/
import java.util.Scanner;
public class EX12R {

	public static void main(String[] args) {
		Scanner tecla = new Scanner(System.in);
        int gi, gg, oj, vi = 0, vg = 0, e = 0, qj = 0;

        do {
        System.out.println("Digite a quantidade de Gols feitos pelo Inter no Grenal: ");
        gi = tecla.nextInt();
        System.out.println("Digite a quantidade de Gols feitos pelo Grêmio no Grenal: ");
        gg = tecla.nextInt();
        if(gi>gg) {
        	System.out.println("O Inter ganhou o Grenal!");
        	vi = vi + 1;
        	}else
        		if(gg>gi) {
        			System.out.println("O Grêmio ganhou o Grenal!");
        			vg = vg + 1;
        		}else {
        			System.out.println("O Grenal Ficou empatado!");
        			e = e +1;
        		}
        System.out.println("Novo Grenal - 1.Sim - 2.Não : ");
        oj = tecla.nextInt();
        qj = qj +1;
        
        }while(oj == 1);
        
        System.out.println("Número de Vitórias do Inter: "+vi);
        System.out.println("Número de Vitórias do Grêmio: "+vg);
        System.out.println("Número de Empates: "+e);
        if(vi>vg) {
        	System.out.println("O Inter é o maior ganhador de Grenais!");
        }else
        	if(vg>vi) {
        		System.out.println("O Grêmio é o maior ganhador de Grenais!");
        	}else
        		if((vg==vi)&&(qj==1)) {
        			System.out.println("Não houve vencedor!");
        		}else {
        			System.out.println("Com base nos jogos informados as equipes estão empatadas e número de vitórias, tendo "+vg+" vitórias cada uma e com "+e+" empates!");
        		}
        			     
        
	}

}
